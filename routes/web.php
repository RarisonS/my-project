<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'site\SiteController@index')->name('home');

Route::get('/admin', 'admin\AdminController@index')->name('admin.home');

Auth::routes();


